var http = require('http');
var net = require('net');
var WebSocket = require('ws');
var WebSocketServer = WebSocket.Server;
var cluster = require('cluster');
var lzjb = require('lzjb');

function inetNtoa(buf) {
  return buf[0] + '.' + buf[1] + '.' + buf[2] + '.' + buf[3];
};

if (cluster.isMaster) {
  cluster.fork();
  cluster.on('exit', function(worker, code, signal) {
    cluster.fork();
  });
}
if (cluster.isWorker) {
  var s = http.createServer();
  s.on('request', function(request, response) {
    response.writeHeader(302, {
      'Location': 'http://www.weimob.com',
      'Node.js': process.version || 'N/A'
    });
    response.end();
  });
  s.on('error', function() {
    return process.exit(1);
  });
  var w = new WebSocketServer({
    server: s
  });
  w.on('connection', function(ws) {
    var stage = 0;
    var headerLength = 0;
    var remote = null;
    var cachedPieces = [];
    var addrLen = 0;
    var remoteAddr = null;
    var remotePort = null;
    ws.on('message', function(data, flags) {
      var addrtype, buf, e, error;
      try {
        data = Buffer.from(lzjb.decompressFile(data));
      } catch(e) {
        ws.close();
        return;
      }
      if (stage === 5) {
        if (!remote.write(data)) {
          ws._socket.pause();
        }
        return;
      }
      if (stage === 0) {
        try {
          addrtype = data[0];
          if (addrtype === 3) {
            addrLen = data[1];
          } else if (addrtype !== 1) {
            ws.close();
            return;
          }
          if (addrtype === 1) {
            remoteAddr = inetNtoa(data.slice(1, 5));
            remotePort = data.readUInt16BE(5);
            headerLength = 7;
          } else {
            remoteAddr = data.slice(2, 2 + addrLen).toString('binary');
            remotePort = data.readUInt16BE(2 + addrLen);
            headerLength = 2 + addrLen + 2;
          }
          remote = net.connect(remotePort, remoteAddr, function() {
            var i, piece;
            i = 0;
            while (i < cachedPieces.length) {
              piece = cachedPieces[i];
              remote.write(piece);
              i++;
            }
            cachedPieces = null;
            return stage = 5;
          });
          remote.on('data', function(data) {
            data = lzjb.compressFile(data);
            if (ws.readyState === WebSocket.OPEN) {
              ws.send(data, {
                binary: true
              });
              if (ws.bufferedAmount > 0) {
                remote.pause();
              }
            }
          });
          remote.on('end', function() {
            ws.close();
          });
          remote.on('drain', function() {
            return ws._socket.resume();
          });
          remote.on('error', function() {
            ws.terminate();
          });
          remote.setTimeout(60000, function() {
            remote.destroy();
            return ws.close();
          });
          if (data.length > headerLength) {
            buf = Buffer.alloc(data.length - headerLength);
            data.copy(buf, 0, headerLength);
            cachedPieces.push(buf);
            buf = null;
          }
          return stage = 4;
        } catch (e) {
          if (remote) {
            remote.destroy();
          }
          return ws.close();
        }
      } else {
        if (stage === 4) {
          return cachedPieces.push(data);
        }
      }
    });
    ws.on('ping', function() {
      if (ws.readyState === WebSocket.OPEN) {
        ws.pong('', false);
      }
    });
    ws._socket.on('drain', function() {
      if (stage === 5) {
        remote.resume();
      }
    });
    ws.on('close', function() {
      if (!!remote) {
        remote.destroy();
      }
    });
    return ws.on('error', function(e) {
      if (!!remote) {
        remote.destroy();
      }
    });
  });
  s.listen(8080);
}